import Vue from 'vue'
//Dòng này để import vue-router
import Router from 'vue-router'
import TodoApp from '@/components/ConCula'
import Newthings from '../components/New-Things/DeMo'
Vue.use(Router)


export default new Router({ 
  mode:'history',
  routes: [ // bao gồm danh sách route
    {
      path: '/', ///path của route
      name: 'todoApp', // tên route
      component: TodoApp // component route sử dụng
    },
    {
      path: '/new-things',
      name: 'new',
      component: Newthings
    }
  ]
})
