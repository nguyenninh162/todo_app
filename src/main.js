import Vue from 'vue'
import App from './App.vue'
// import VueRouter from './router/index'
import router from './router/index'

import BootstrapVue from 'bootstrap/dist/css/bootstrap.css'

Vue.use(BootstrapVue)

Vue.config.productionTip = false

new Vue({
  // VueRouter,
  router,
  render: h => h(App),
}).$mount('#app')
